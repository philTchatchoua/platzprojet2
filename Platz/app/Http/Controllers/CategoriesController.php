<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Categorie;

class CategoriesController extends Controller
{
    public function index(){
        $categories = Categorie::orderBy('id', 'DESC')->get();
        return response()->json($categories);
    }
}
