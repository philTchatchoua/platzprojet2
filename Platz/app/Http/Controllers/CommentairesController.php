<?php
/*
    Chemin :app/Http/Controllers/PostsController.php
    Description: Controller des comentaires
    Données disponible: -- // --
*/


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Commentaire;

class CommentairesController extends Controller
{   
    /**
    * type: function
    * nom:  index
    * Desc: ontroleur des commentaires
    */
    public function index(){

        $commentaires = Commentaire::all();
        return response()->json($commentaires);

    }

    /**
     * type: function
     * nom:  store
     * Desc: save les commentires dans la db
     */
    public function store(Request $request){

        $commentaire = Commentaire::create($request->all());
        return response()->json($commentaire);
    }
}
