<?php
/*
    Chemin :app/Http/Controllers/PostsController.php
    Description: Controller des poss
    Données disponible: -- // --
*/


namespace App\Http\Controllers;

use App\Http\Models\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{   
    /**
    * type: function
    * nom: index
    * Desc:liste des posts
    */
    public function index(){

        if(request('p')!= null){
            $posts = Post::where('titre','like','%'.request('p').'%')->get();
            return response()->json($posts);
        }else{
            $posts = Post::with('categories','commentaires')->orderBy('id','DESC')->get();
            return response()->json($posts);
        }

    }
    
}
