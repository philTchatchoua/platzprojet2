<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    /**
     * type: function
     * nom: post
     * Desc:liaison 1-n entre les table posts - categories
     */

     public function Post()
     {
        return $this->hasMany('App\Http\Models\Post', 'categories_id');
     }
}
