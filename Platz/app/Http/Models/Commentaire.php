<?php

/*
    Chemin :app/Http/Models/Commentaire.php
    Description: Models des commentaires
    Données disponible: -- // --
*/

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{

   protected $guarded=[];
   public $timestamps= false;
    /**
    * type: function
    * nom:  post
    * Desc: liaison 1-n
    */
    public function post()
    {
        return $this->belongsTo('App\Http\Models\Post','posts_id','id');
    }
    
}
