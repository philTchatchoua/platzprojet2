<?php
/*
    Chemin :app/Http/Models/Post.php
    Description: Models des posts
    Données disponible: -- // --
*/
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
    * type: function
    * nom: categories
    * Desc:liaison 1-n entre les table posts - categories
    */
    public function categories()
    {
        return $this->belongsTo('App\Http\Models\Categorie','categories_id');
    }

    /**
     * type: function
     * nom:  commentaires
     * Desc:liaison 1-n entre les table posts - commentaires
     */
    public function commentaires()
    {
        return $this->hasMany('App\Http\Models\Commentaire','posts_id','id');
    }
   
}
