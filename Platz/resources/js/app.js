/*
    Chemin: /resources/js/app.js
    Description: instanciation vue
    Données disponible: -- // --
*/

require('./bootstrap');

window.Vue = require('vue');

//Chargement des composants
Vue.component('navigateur', require('./components/navBar/Nav.vue').default);
//Vue.component('searchPosts', require('./components/navBar/Search.vue').default);

//Chargement des fichiers
import router from './router.js'
import store from './store/index.js'

const app = new Vue({
    el: '#app',
    router,
    store,
    created : function()  {
         return this.$store.dispatch('setPosts');
    },

    mounted : function(){
        return this.$store.dispatch('setCategories');
    }
   
});
