/*
    Chemin :/resources/views/templates/partials/_head.blade.php
    Description: Templates par default
    Données disponible: -- // --
*/

import Vue from 'vue'
import Router from 'vue-router'

// Chargement des composants des différentes routes
import PostsIndex from './components/posts/Index'
import PostsShow from './components/posts/Show'
import PostsByCat from './components/posts/IndexByCat'


// Création du routing
Vue.use(Router)
export default new Router({
    routes: [
        {
            path:'/',
            name: 'posts.index',
            component: PostsIndex
        },
        {
            path: 'posts/:id',
            name: 'posts.show',
            component: PostsShow
        },
         {
             path: 'postsByCat/:cat',
             name: 'posts.indexByCat',
             component: PostsByCat
         },
    ]
})

