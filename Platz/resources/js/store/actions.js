/*
    Chemin :/resources/js/store/actions.js
    Description: Récupère des data de la db via api
    Données disponible: -- // --
*/

let actions = {
    setPosts: function ({commit}) {
        //Transaction Ajax
        axios.get('api/posts')
        .then(reponsePHP => commit('SET_POSTS',reponsePHP.data))
    },
    setCategories: function ({commit}) {
        //Transaction Ajax
        axios.get('api/categories')
            .then(reponsePHP => commit('SET_CATEGORIES', reponsePHP.data))
    },
}

export default actions