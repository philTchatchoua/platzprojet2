/*
    Chemin :/resources/js/store/getters.js
    Description: Permet de récupérer  les data dans le store
    Données disponible: -- // --
*/

let getters = {
    getPosts: function (state) {
         return state.posts;
    },
    getPostById: function(state){
        return function (id){
            return state.posts.find(post=>post.id == id);
        }
    },
     getPostByCat: function (state) {
         return function (cat) {
             return state.posts.filter(post => post.categories.cat == cat);
         }
     },
    getCategories: function(state) {
        return state.categories;
    }
}

export default getters;
