/*
    Chemin :/resources/js/store/index.js
    Description: Maêtre d'orchestre du folder store
    Données disponible: -- // --
*/


import Vue from 'vue'
import Vuex from 'vuex'

//chargements des fichiers 
import state     from './state.js'
import getters   from './getters.js'
import mutations from './mutations.js'
import actions   from './actions.js'

Vue.use(Vuex);
export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations
});

