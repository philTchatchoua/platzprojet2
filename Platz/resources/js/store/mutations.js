/*
    Chemin :/resources/js/store/mutations.js
    Description: manipulele des data provenants de la actions
    Données disponible: -- // --
*/

let mutations = {
    SET_POSTS: function (state,data) {
        return (state.posts= data);
    },
    
    SET_CATEGORIES:function(state,data){
        return (state.categories = data);
    }
}

export default mutations;
