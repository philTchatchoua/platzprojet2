<!DOCTYPE HTML>
<html>

<head>
    @include('templates.partials._head')
</head>

<body>
    <!-- HEADER /NAVBAR  -->
    {{-- @include('templates.partials._HeaderNav') --}}
    
    <!-- FILTER -->
    {{-- @include('templates.partials._filter') --}}

    <!-- PORTFOLIO -->
    @include('templates.partials._body')

    <!-- FOOTER-->
    @include('templates.partials._footer')

    <!-- SCRIPT -->
    @include('templates.partials._script')
</body>

</html>
