{{--
    Chemin :/resources/views/templates/partials/_headerNav.blade.php
    Description: Templates par default du nav et header
    Données disponible: -- // --
--}}


<a name="ancre"></a>
<!-- CACHE -->
<div class="cache"></div>
<!-- HEADER -->
<div id="wrapper-header">
    <div id="main-header" class="object">
        <div class="logo"><img src="img/logo-burst.png" alt="logo platz" height="38" width="90"></div>
        <div id="main_tip_search">
            <form>
                <input type="text" name="search" id="tip_search_input" list="search" autocomplete=off required>
            </form>
        </div>
        <div id="stripes"></div>
    </div>
</div>
<!-- NAVBAR-->
<div id="wrapper-navbar">
    <div class="navbar object">
        <div id="wrapper-bouton-icon">
            <div id="bouton-ai"><img src="img/icon-ai.svg" alt="illustrator" title="Illustrator" height="28" width="28">
            </div>
            <div id="bouton-psd"><img src="img/icon-psd.svg" alt="photoshop" title="Photoshop" height="28" width="28">
            </div>
            <div id="bouton-theme"><img src="img/icon-themes.svg" alt="theme" title="Theme" height="28" width="28">
            </div>
            <div id="bouton-font"><img src="img/icon-font.svg" alt="font" title="Font" height="28" width="28"></div>
            <div id="bouton-premium"><img src="img/icon-premium.svg" alt="premium" title="Premium" height="28"
                    width="28"></div>
        </div>
    </div>
</div>
