<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//  ROUTES DES POSTS ------------------------------------------------------------
/*
  DESC: Routes donnant acccès aux données des posts 
  PATTERN: +sieurs 
  CTRL: PostsController
  ACTION: ---//---
*/

Route::resource('posts','PostsController',[
    'except' =>['show','create','edit'
    ]
]);

//  ROUTES DES CATEGORIES ------------------------------------------------------------
/*
  DESC: Routes donnant acccès aux données des categories
  PATTERN: +sieurs 
  CTRL: CategoriesController
  ACTION: ---//---
*/
Route::resource('categories', 'CategoriesController', [
  'except' => ['show', 'create', 'edit']
]);


//  ROUTES DES COMMENTAIRES------------------------------------------------------------
/*
  DESC: Routes donnant acccès aux données commentaires
  PATTERN: +sieurs 
  CTRL: CommmentairesController
  ACTION: ---//---
*/
Route::resource('commentaires', 'CommentairesController', [
  'except' => ['show', 'create', 'edit']
]);


